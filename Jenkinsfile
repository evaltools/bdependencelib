#!groovy

pipeline {
  agent {
    node {
      label 'VisualStudio2017'
    }
  }
  stages {
    stage('Prepare Repository') {
        when { expression { BRANCH_NAME ==~ /^PR-[0-9]*/ }  }
        steps {
            bat 'git remote remove upstream'
        }
    }
    stage('build') {
      parallel {
        stage('compile_debug') {
          steps {
            bat 'build -configuration=Debug'
          }
        }
        stage('compile_release') {
          steps {
            bat 'build'
          }
        }
      }
    }
    stage('test') {
      parallel {
        stage('OpenCover') {
          steps {
            bat 'build -target=OpenCover'
            publishHTML (target: [
              allowMissing: true,
              alwaysLinkToLastBuild: false,
              keepAll: false,
              reportName: "OpenCover Report",
              reportDir: 'TestResults',
              reportFiles: 'CoverResult.html'
              ])
          }
        }
      }
    }
    stage('static_analysis') {
      parallel {
        stage('DupFinder') {
          steps {
            bat 'build -target=DupFinder'
            publishHTML (target: [
              allowMissing: false,
              alwaysLinkToLastBuild: false,
              keepAll: false,
              reportName: "ReSharper DupFinder Report",
              reportDir: 'TestResults/ReSharperReports',
              reportFiles: 'dupfinder-output.html'
              ])
          }
        }
        stage('InspectCode') {
          steps {
            bat 'build -target=Restore'
            bat 'build -target=InspectCode'
            publishHTML (target: [
              allowMissing: false,
              alwaysLinkToLastBuild: false,
              keepAll: false,
              reportName: "ReSharper InspectCode Report",
              reportDir: 'TestResults/ReSharperReports',
              reportFiles: 'inspectcode-output.html'
              ])
          }
        }
        stage('SonarQube analysis') {
          steps {
            withSonarQubeEnv('SonarQubeServer') {
              bat 'build -target=Sonar -sonarHostUrl=%SONAR_HOST_URL% -sonarLogin=%SONAR_AUTH_TOKEN%'
            }
            timeout(time: 1, unit: 'HOURS') {
              waitForQualityGate abortPipeline: true
            }
          }
        }
      }
    }
    stage('Deploy') {
      when { tag "v*" }
      steps {
        withCredentials([string(credentialsId: 'NuGetApiKey', variable: 'NuGetApiKey')]) {
          bat 'build -target=Deploy -nugetSource=%NuGetSource% -nugetApiKey=%NuGetApiKey%'
        }
      }
    }
  }
  environment {
    NuGetSource = 'http://eval-jira.eastasia.cloudapp.azure.com:8081/repository/nuget-hosted/'
  }
}
