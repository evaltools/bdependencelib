using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Moq;
using ainterfacelib;

namespace bdependencelib.Unit.Tests
{
    [TestFixture]
    public class CBlogDbServiceUnitTest
    {
        private CBlogDbService _sut;

        private readonly Mock<DbSet<Blog>> _mockSetBlogs = new Mock<DbSet<Blog>>();
        private readonly Mock<CBlogContext> _mockBlogContext = new Mock<CBlogContext>();

        private SqliteConnection _connectionForIntegrationTest;
        private CBlogContext _contextUsingMemSqliteForIntegrationTest;

        private Blog _blogExample;
        private Post _postExample;

        [SetUp]
        public void SetUp()
        {
            _mockSetBlogs.Reset();
            _mockBlogContext.Reset();
            _mockBlogContext.Setup(m => m.Blogs).Returns(_mockSetBlogs.Object);

            _sut = new CBlogDbService(_mockBlogContext.Object);

            _blogExample = new Blog()
            {
                Url = @"http://blogs.com/blog/1",
                Rating = 5
            };

            _postExample = new Post()
            {
                Title = "Test Title",
                Content = "Some Content"
            };
        }

        [TearDown]
        public void TearDown()
        {
            ClearDbContext();
        }

        [Test]
        public void Test_CBlogDbService_Default_Constructor_Success()
        {
            Assert.That(_sut, Is.Not.Null);
        }

        #region Private Method

        private void ClearDbContext()
        {
            _contextUsingMemSqliteForIntegrationTest?.Dispose();
            _connectionForIntegrationTest?.Dispose();

            _contextUsingMemSqliteForIntegrationTest = null;
            _connectionForIntegrationTest = null;
        }

        #endregion
    }
}
