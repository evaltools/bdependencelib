﻿using Microsoft.EntityFrameworkCore;
using ainterfacelib;

namespace bdependencelib
{
    public class CBlogContext : DbContext
    {
        public virtual DbSet<Blog> Blogs { get; set; }
        public virtual DbSet<Post> Posts { get; set; }

        public CBlogContext()
        {
        }

        public CBlogContext(DbContextOptions options) : base(options)
        {
        }
    }
}
