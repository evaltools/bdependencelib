﻿using Microsoft.EntityFrameworkCore;
using ainterfacelib;

namespace bdependencelib
{
    public class CBlogDbService : IBlogDbService
    {
        public CBlogDbService(CBlogContext dbContext)
        {
            _dbContext = dbContext;
        }

        private readonly CBlogContext _dbContext;

        public void Migrate()
        {
            _dbContext.Database.Migrate();
        }

        public int CreateBlog(Blog blog)
        {
            var entityEntry = _dbContext.Blogs.Add(blog);
            _dbContext.SaveChanges();
            return entityEntry.Entity.BlogId;
        }

        public Blog ReadBlog(int blogId)
        {
            throw new System.NotImplementedException();
        }

        public int UpdateBlog(Blog blog)
        {
            throw new System.NotImplementedException();
        }

        public int DeleteBlog(int blogId)
        {
            throw new System.NotImplementedException();
        }

        public int CreatePost(Post post)
        {
            throw new System.NotImplementedException();
        }

        public Post ReadPost(int postId)
        {
            throw new System.NotImplementedException();
        }

        public int UpdatePost(Post blog)
        {
            throw new System.NotImplementedException();
        }

        public int DeletePost(int postId)
        {
            throw new System.NotImplementedException();
        }
    }
}
